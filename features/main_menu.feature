Feature: As a user I want to convert units

  @leftsidemenu
  Scenario: When I tap on menu icon, I should see left side menu
    Given I land on home screen
    #* I press on menu icon
    When I press on menu icon
    Then I should see left side menu

#  Scenario: When I visit conversions for the first time it should be empty
#    Given I land on home screen
#    And I press on menu icon
#    When Tapping on My Conversions
#    Then I should see the text for empty conversions

  @conversions
  Scenario: I should be able to open My Conversions screen
    Given I land on home screen
    When I press on menu icon
    And I press on My Conversions button
    Then I land on My Conversions screen