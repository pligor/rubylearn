Feature: User is able to create and manage custom conversions

  @custom_conversions
  Scenario: User able to create new conversion
    Given I land on home screen
    When I press on menu icon
    And I press on My Conversions button
    And I press on Create first conversion button
    And I type "Power" as custom conversion name
    And I press on New Unit button
    And I type "Horse power" as unit name
    And I type "HP" as unit symbol
    And I type "1" as unit value
    # I want to specify the checkmark for only this screen:
    And I press submit checkmark on Custom conversions screen

    And I press on New Unit button
    And I type "Mule power" as unit name
    And I type "MP" as unit symbol
    And I type "0.5" as unit value
    # I want to specify the checkmark for only this screen:
    And I press submit checkmark on Custom conversions screen
    And I confirm the creation of a new Custom Conversion
    Then I verify "Power" is added to Custom conversions List