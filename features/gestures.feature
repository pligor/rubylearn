Feature: User is able to use gestures

  Background:
    Given I land on home screen

  @swipemenu
  Scenario: User able to swipe to open left side menu
    When I swipe from left to right
    Then I should see left side menu
    When I swipe from right to left
    Then I see "Length" as a current unit conversion

  @swipecalc
  Scenario: User able to swipe to open calculator
    When I swipe from right to left
    Then I see "Calculator" as a current unit conversion
    When I swipe from left to right
    Then I see "Length" as a current unit conversion

