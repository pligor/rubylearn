require 'appium_lib'
require 'pry'

=begin
adb devices
adb shell pm list packages    #get the package name from this command (i.e. com.samsung.android.app.memo)
adb shell pm path com.samsung.android.app.memo   #get where the apk of this package is located. i.e. /data/app/com.android.chrome-2/base.apk
adb pull /data/app/com.samsung.android.app.memo-1/base.apk   #download the apk

#open the application in the device and only then execute this to get the app package and app activity:
adb shell dumpsys window windows | grep -E 'mCurrentFocus|mFocusedApp'

you get something like this:

p -E 'mCurrentFocus|mFocusedApp'
  mCurrentFocus=Window{ecf0983 u0 d0 com.samsung.android.app.memo/com.samsung.android.app.memo.Main}
  mFocusedApp=AppWindowToken{d0764decc token=Token{934d0ff ActivityRecord{46f301e u0 com.samsung.android.app.memo/.Main t2206}}}


app package is here:                     ^^^^^^^^^^^^^
and app activity is here:                                                                      ^^^
=end

#doc documentation: https://www.rubydoc.info/github/appium/ruby_lib/Appium/Android/Uiautomator2/Element
def caps #capabilities
  #apppath = File.join(File.dirname(__FILE__), 'PreciseUnitConversion.apk')
  apppath = File.join(File.dirname(__FILE__), 'memo.apk')

  raise Exception.new("file missing") unless File.exists?(apppath)

  {
      caps: {
          deviceName: "Anyname",
          platformName: "Android",
          app: apppath,
          #appPackage: "com.ba.universalconverter",
          appPackage: "com.samsung.android.app.memo",

          # appActivity: "MainConverterActivity",
          appActivity: "Main",

          newCommandTimeout: "3600",
      }
  }
end

Appium::Driver.new(caps, true)

#all appium library methods available inside our step definitions
Appium.promote_appium_methods(Object)
