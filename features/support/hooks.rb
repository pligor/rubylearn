#https://docs.cucumber.io/cucumber/api/#hooks

Before do
  puts("The BeforeHook is executed before EVERY scenario")
  #puts($driver.methods)
  $driver.start_driver
end

After {|scenario|
  if scenario.failed?
    dirname = 'screenshots'
    #take screenshot
    if File.directory?(dirname)
      #nop
    else
      FileUtils.mkdir_p(dirname)
    end

    timestamp = Time.now.strftime("%Y-%m-%d_%H.%M.%S")
    screenshot_name = timestamp + ".png"
    screenshot_path = File.join(dirname, screenshot_name)

    $driver.screenshot(screenshot_path) #money shot here! save screenshot
    embed(screenshot_path.to_s, "image/png") #this is to embed the screenshot in the report! That's interesting

  end

  sleep 3 #sleep a few seconds before closing Android application to be able and see the result
  $driver.driver_quit
}

AfterConfiguration { #this is being executed at the very beginning when we have setup the configuration
  puts("AFTER CONFIGURATION")
  dirname = 'screenshots'
  FileUtils.rm_r(dirname) if File.directory?(dirname)
}