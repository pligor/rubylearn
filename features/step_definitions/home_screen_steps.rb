require_relative '../../helpers/home_screen_helpers'

def check_unit_picker(side, spinner_direction, value)
  #from_spinner.text() this is not for searching
  #my_elem = from_spinner.find_element(class_chain: 'android.widget.TextView') #this is for iOS only

  spinner = find_element(id: spinner_direction + '_units_spinner')
  #from_spinner.text() this is not for searching
  #my_elem = from_spinner.find_element(class_chain: 'android.widget.TextView') #this is for iOS only
  actual_text = spinner.find_element(uiautomator: 'new UiSelector().className("android.widget.TextView")').text
  fail("expected #{side} unit picker value is: #{value} but actual was #{actual_text}") unless actual_text == value

  actual_text
end


Then(/^Left Unit picker value should be "([^"]*)"$/) do |value|
  side = 'left'
  check_unit_picker(side = side, spinner_direction = 'from', value = value)

  #puts("alternatively")
  actual_text = find_elements(id: 'select_unit_spinner')[0].text #but more error prone because it is based on indices
  fail("expected #{side} unit picker value is: #{value} but actual was #{actual_text}") unless actual_text == value

end

And(/^Right unit picker value should be "([^"]*)"$/) do |value|
  side = 'right'
  check_unit_picker(side, spinner_direction = 'to', value = value)

  #puts("alternatively")
  actual_text = find_elements(id: 'select_unit_spinner')[1].text #but more error prone because it is based on indices
  fail("expected #{side} unit picker value is: #{value} but actual was #{actual_text}") unless actual_text == value
end

Then(/^Show All button should be (enabled|disabled|undefined)$/) do |state|
  if state == 'undefined'
    fail('the state should never be undefined, this was just to try stuff')
  else
    btn_show_all = find_element(id: 'btn_show_all')
    actual = btn_show_all.enabled?
    # puts btn_show_all.respond_to?('enabled')
    # puts btn_show_all.respond_to?('aaa')
    # puts("GAMIESAI")
    fail("expected #{state} but the actual was #{actual ? 'enabled' : 'disabled'}") unless actual == (state == 'enabled')
  end
end

When(/^I press on C \(clear\) button$/) do
  use_soft_keyboard("C")
end

When(/^I type "([^"]*)" to app's own keyboard$/) do |arg|
  use_soft_keyboard(arg)
end

Then(/^I should see result as "([^"]*)"$/) do |arg|
  actual = find_element(id: 'target_value').text
  fail("expected #{arg} but actual was #{actual}") unless actual == arg
end

When(/^I press on Add to Favorites icon$/) do
  find_element(id: 'action_add_favorites').click
end

And(/^I press on Favorite conversions$/) do
  text('Favorite conversions').click
end

Then(/^I verify "([^"]*)" is added to Favorite conversions lists$/) do |arg|
  actual = find_element(id: 'favorites_item_hint').text
  fail("Were expecting #{arg} but actually got #{actual}") unless actual == arg
end

When(/^I press on search icon$/) do
  find_element(id: 'action_bar').find_element(id: 'action_search').click
end

And(/^I type "([^"]*)" in search field$/) do |search_query|
  find_element(id: 'search_src_text').send_keys(search_query)
end

And(/^I press return button on soft keyboard$/) do
  #$driver.pressKeyCode(AndroidKeyCode.KEYCODE_NUMPAD_ENTER);
  # this taps at the 99% x and y axis on the screen with the assumption that the softkeyboard will always have the
  # return button at the bottom right corner which is not a good assumption and can easily break
  # really bad way, but at least we know how to do a tap without elements
  #Appium::TouchAction.new.tap(x:0.99, y:0.99, count: 1).perform

  raise "press_keycode is not working for current driver" unless $driver.respond_to?('press_keycode')
  $driver.press_keycode(66)
end

Then(/^I see "([^"]*)" as a current unit conversion$/) do |unit_name|
  #text(unit_name) this is all over the screen, we want something better
  find_element(id: 'action_bar').find_element(xpath: "//android.widget.TextView[@text='#{unit_name}']")
end


But(/^Not see "([^"]*)" or "([^"]*)" as a current unit conversion$/) do |arg1, arg2|
  actual_text = find_element(id: 'action_bar').find_element(xpath: "//android.widget.TextView").text

  fail("the left and right units should not be the name of conversion") if actual_text == arg1 || actual_text == arg2
end

def wait_for(seconds)
  Selenium::WebDriver::Wait.new(timeout: seconds).until {yield}
end

When(/^I select "([^"]*)" from (left|right) unit picker$/) do |arg, side|
  find_element(id: side == 'left' ? 'from_units_spinner' : "to_units_spinner").click

  duration_msec = 600

  text_first_element = lambda {
    find_element(id: 'select_dialog_listview').find_element(xpath: "//android.widget.TextView").text
  }

  scroll_now = lambda {|direction|
    puts("SCROLLING #{direction}")
    percent = 0.4
    start_y = direction == 'up' ? percent : 1 - percent
    end_y = 1 - start_y
    Appium::TouchAction.new.swipe(start_x: 0.5, start_y: start_y, end_x: 0.5, end_y: end_y, duration: duration_msec).perform
  }

  go_to_top = lambda {
    prev_elem = nil
    # counter = 0
    until text_first_element.call == prev_elem
      prev_elem = text_first_element.call
      scroll_now.call('up')
    end
    # puts("COUNTER")
    # puts(counter)
  }

  go_to_top.call

=begin
  try_get_elem = lambda {
    begin
      # wait_for(3) {
      #   return text(arg)
      # }
      saved_wait = $driver.appium_wait_timeout
      $driver.set_wait(2)
      return text(arg)
    rescue Selenium::WebDriver::Error::NoSuchElementError
      return nil
    ensure
      $driver.set_wait(saved_wait)
    end
  }

  prev_elem = nil
  elem_option = nil
  until text_first_element.call == prev_elem
    elem_option = try_get_elem.call
    break if elem_option

    prev_elem = text_first_element.call
    scroll_now.call('down')
  end
  #try one last time
  elem_option = try_get_elem.call if elem_option == nil

  if elem_option
    elem_option.click
  else
    raise Selenium::WebDriver::Error::NoSuchElementError("element with text #{arg} could NOT be found")
  end
=end
  saved_wait = $driver.appium_wait_timeout
  $driver.set_wait(2) #secs

  #get_source <--- you can also use this function to get current state of screen (all xml code, if no further scrolling
  # it will not have changed)
  prev_elem = nil
  until exists {text(arg)}
    break if prev_elem == text_first_element.call #we cannot scroll any further

    prev_elem = text_first_element.call
    scroll_now.call('down')
  end
  $driver.set_wait(saved_wait)

  text(arg).click
end

And(/^I select "([^"]*)" from menu$/) do |value|
  text(value).click
end


When(/^I click on switch button$/) do
  find_element(id: 'img_switch').click
end

And(/^I should see "([^"]*)" prompt in history list$/) do |prompt|
  actual = find_element(id: 'text_info_history').text
  fail("Expected #{prompt} but actually got: #{actual}") unless actual == prompt
end

When(/^I press delete icon on "(\d+)"\-th result in history list$/) do |index|
  #index-1 because in scenario we are counting 1, 2, 3, 4, 5, ...
  actual_index = [index.to_i - 1, 0].max
  find_elements(id: 'deleteIcon')[actual_index].click
end

Then(/^I verify that "(\d+)"\-th result in history list is "([^"]*)"$/) do |index, unit_name|
  actual_index = [index.to_i - 1, 0].max
  actual = find_elements(id: 'history_item_hint')[actual_index].text
  fail("Expected #{prompt} but actually got: #{actual}") unless actual == unit_name
end

When(/^I press on clear history button$/) do
  find_element(id: 'btn_clear_history').click
  find_element(id: 'button1').click
end