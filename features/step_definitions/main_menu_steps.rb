#Due to promote Appium methods we have ALL appium library methods available inside our step definitions

Given("I land on home screen") do
  #pending # Write code here that turns the phrase above into concrete actions
  # you CANNOT share variables just like that between steps

  # assumption: we are saying that if you find these two elements then we should be in home screen
  # if you give the wrong elements or the elements cannot be found then you will get an error!
  find_element(id: 'action_search')
  #this is given as the last part of com.ba.universalconverter:id/action_add_favorites from resource id
  find_element(id: 'action_add_favorites')
  puts("elements found therefore we should be in home screen")
end


When(/^I press on menu icon$/) do
  sandwich_menu_button = find_element(accessibility_id: 'Open navigation drawer')
  sandwich_menu_button.click
  #puts aa # you CANNOT share variables just like that between steps
end

Then(/^I should see left side menu$/) do
  #find_element(text: 'Unit Converter')
  text('Unit Converter')
  puts 'left side menu is here'
end

And(/^I press on My Conversions button$/) do
  text('My conversions').click
  puts("conversion button pressed")
end

Then(/^I land on My Conversions screen$/) do
  text('No personal conversion created yet')
  puts("landed on my conversions screen")
end