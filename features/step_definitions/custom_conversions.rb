And(/^I press on Create first conversion button$/) do
  find_element(id: 'btn_new_custom_conversion').click
end

And(/^I type "([^"]*)" as custom conversion name$/) do |conv_name|
  find_element(id: 'edit_custom_conversion_category_name').send_keys(conv_name)
end

And(/^I press on New Unit button$/) do
  find_element(id: 'btn_new_custom_unit').click
end

And(/^I type "([^"]*)" as unit name$/) do |arg|
  find_element(id: 'edit_custom_conversion_unit_dtls_name').send_keys(arg)
end

And(/^I type "([^"]*)" as unit symbol$/) do |arg|
  find_element(id: 'edit_custom_conversion_unit_dtls_symbol').send_keys(arg)
end

And(/^I type "([^"]*)" as unit value$/) do |arg|
  find_element(id: 'edit_custom_conversion_unit_dtls_value').send_keys(arg)
end

And(/^I press submit checkmark on Custom conversions screen$/) do
  find_element(id: 'action_confirm_custom_unit').click
end

And(/^I confirm the creation of a new Custom Conversion$/) do
  find_element(id: 'action_confirm_custom_category').click
  #find_element(id: '# btn_custom_conversion_details_ok').click #<--- this didn't work so well
end

Then(/^I verify "([^"]*)" is added to Custom conversions List$/) do |arg|
  text(arg)
end