def swiping(direction)
  puts("SCROLLING #{direction}")
  duration_msec = 600
  percent = 0.95
  start_x = direction.downcase == 'left' ? percent : 1 - percent
  end_x = 1. - start_x
  Appium::TouchAction.new.swipe(start_x: start_x, start_y: 0.5,
                                end_x: end_x, end_y: 0.5, duration: duration_msec).perform
end

When(/^I swipe from left to right$/) do
  swiping(direction = 'right')
end


When(/^I swipe from right to left$/) do
  swiping(direction = 'left')
end
