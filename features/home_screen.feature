Feature: Tests for Home Screen functionality

  Background:
    Given I land on home screen

  @cleanhist
  Scenario: User is able to cleanup conversion history
    When I press on menu icon
    And I select "History" from menu
    Then I see "History" as a current unit conversion
    And I should see "No history right now" prompt in history list
    When I press on menu icon
    And I select "Length" from menu
    And I type "1" to app's own keyboard
    And I press on menu icon
    And I select "History" from menu
    Then I verify that "1"-th result in history list is "Length"
      #When I press delete icon on "1"-th result in history list
    When I press on clear history button
    Then I should see "No history right now" prompt in history list

  @switchvals
  Scenario: User able to switch values
    #it is now provided from Background section of cucumber Given I land on home screen
    Then Left Unit picker value should be "Foot"
    And Right unit picker value should be "Centimeter"
    When I click on switch button
    Then Left Unit picker value should be "Centimeter"
    And Right unit picker value should be "Foot"

  @favoritetag
  @canconv
  Scenario: User can convert values
    #it is now provided from Background section of cucumber Given I land on home screen
    When I press on menu icon
    And I select "Volume" from menu
    When I select "Cup" from right unit picker
    And I type "1" to app's own keyboard
    Then I should see result as "15.1416"

  @favoritetag
  @selunit
  Scenario Outline: User able to select another unit
    #it is now provided from Background section of cucumber Given I land on home screen
    When I select "<unit_type>" from left unit picker
    And I type "<input>" to app's own keyboard
    Then I should see result as "<output>"
    Examples:
      | unit_type       | input | output |
      | Inch            | 1     | 2.54   |
      | [Hist.rus.] Dot | 1     | 0.0254 |

  @favs
  Scenario: User able to add current conversion to Favorites list
    #it is now provided from Background section of cucumber Given I land on home screen
    When I press on Add to Favorites icon
    And I press on menu icon
    And I press on Favorite conversions
    Then I verify "Length" is added to Favorite conversions lists

  @default
  Scenario: Default values on Home screen is Foot and Centimeter
    #it is now provided from Background section of cucumber Given I land on home screen
    #this are two parameters here passed to the step which is awesome
    Then Left Unit picker value should be "Foot"
    And Right unit picker value should be "Centimeter"

  @showall
  Scenario: Show all button should be disabled at launch, enabled after typing and disabled again by clearing
    #it is now provided from Background section of cucumber Given I land on home screen
    #note here the parameter is NOT inside double quotes, because it can get only a couple of values
    Then Show All button should be disabled
    When I type "0" to app's own keyboard
    Then Show All button should be enabled
    When I press on C (clear) button
    Then Show All button should be disabled

    #it does NOT matter in which ruby file we have organized our step definitions,
    # any .rb file inside the step_definitions folder will do

  @scenout
  Scenario Outline: Verify default conversion
    #it is now provided from Background section of cucumber Given I land on home screen
    When I type "<target>" to app's own keyboard
    Then I should see result as "<result>"
    Examples:
      | target | result    |
      | 1      | 30.48     |
      | 2      | 60.96     |
      | 1051   | 32 034.48 |
      | 10     | 304.8     |

  @searching
  Scenario: User is able to search for existing conversion type
    #it is now provided from Background section of cucumber Given I land on home screen
    When I press on search icon
    And I type "Temperature" in search field
    And I press return button on soft keyboard
    Then I see "Temperature" as a current unit conversion
    And Left Unit picker value should be "Celsius"
    And Right unit picker value should be "Fahrenheit"
    But Not see "Celsius" or "Fahrenheit" as a current unit conversion

