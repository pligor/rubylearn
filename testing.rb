testing = 'okey'.split('')

testing.each do
|char|
  puts(char)
end

#puts "okey\ndokey"
puts 'I "said" this with signs'
puts "complex number of chickens #{33 / 2.0}"


puts ['gemato', 'malakies'].include? 'malakies'

aa = true

if aa
  puts "yep"
else
  puts "not here"
end

arr = [333, 55, 2, 55, 23, 2]

def uniques_ruby(array)
  result = []
  array.each {|elem|
    result.push(elem) unless result.include?(elem)
  }
  result
end

print uniques_ruby(arr)
puts ""

(1..3).each {
    |num|
  puts num
}

class Kaka
  puts self

  aa = "one" #you cannot see the variable aa from global scope

  puts "You are the #{aa}"
end


puts "fff".start_with? "f"

class String
  def has_ma_name?()
    self.downcase.include? "pligoropoulos"
  end
end

puts "sss".has_ma_name?
puts "George Pligoropoulos".has_ma_name?

print Math::constants
puts Math::sqrt(36)

def wrapper(arr)
  res = []
  arr.each {
    |ii|
    res.push [ii, Math::sqrt(ii)] if yield ii #yield makes it require the call of a block
  }
  res
end

print wrapper((1..11)) { |num|
  num % 2 ==1
}
puts ""

#doubler = lambda { |num| num * 2}

#puts doubler.call(10)
my_list = ((1..5).to_a + [4, nil, 33]).map {|item| item == nil ? [] : [item]}
puts my_list.join "."
puts my_list.reject(&:empty?).map {|item| item[0]} .map(&:to_f).join ","