def use_soft_keyboard(num)
  #binding.pry

  keypad = find_element(id: 'keypad')
  nums = num.to_s.split()
  nums.each do
  |cur_num|
    keypad.find_element(xpath: "//android.widget.Button[@text='#{cur_num}']").click
  end
end

def press_back_button()
  #https://discuss.appium.io/t/how-to-click-on-home-back-and-recent-button-in-android/3348
  $driver.press_keycode(4)
end